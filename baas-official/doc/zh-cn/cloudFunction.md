# 云函数

云函数是云引擎的一个子模块，当数据表的常规操作满足不了需求时创建。用户只需要编写服务端的云函数业务逻辑，而服务端的负载均衡等由云引擎提供支持。

#### 云函数模版

```js
async function __[函数名]__(ctx, next, module) {
  	// ToDo
}
```

#### 云函数方法

##### 基础方法（ ctx ）

| 方法               | 说明                                       | 示例                                       |
| ---------------- | ---------------------------------------- | ---------------------------------------- |
| header           | 获取或设置请求标头对象                              | ctx.header                               |
| headers          | 获取或设置请求标头对象                              |                                          |
| method           | 获取或设置请求方法                                | ctx.method                               |
| url              | 获取或设置获取请求 URL                            | ctx.url                                  |
| originalUrl      | 获取请求原始URL                                |                                          |
| origin           | 获取URL的来源                                 |                                          |
| href             | 获取完整的请求URL，包括 `protocol`，`host` 和 `url`。 |                                          |
| path             | 获取或设置请求路径名                               | ctx.path                                 |
| query            | 获取或设置解析的查询字符串, 当没有查询字符串时，返回一个空对象         |                                          |
| params           | 获取或设置链接参数                                |                                          |
| querystring      | 获取或设置根据 `?` 获取原始查询字符串                    |                                          |
| host             | 获取当前主机（hostname:port）                    |                                          |
| hostname         | 存在时获取主机名                                 |                                          |
| fresh            | 检查请求缓存是否“新鲜”                             |                                          |
| stale            | 与 `fresh` 相反                             |                                          |
| socket           | 返回请求套接字                                  |                                          |
| protocol         | 返回请求协议，“https” 或 “http”                  |                                          |
| secure           | 通过 `ctx.protocol == "https"` 来检查请求是否通过 TLS 发出 |                                          |
| ip               | 请求远程地址                                   |                                          |
| ips              | 返回ip数组，从上游 - >下游排序                       |                                          |
| subdomains       | 返回子域名数组                                  |                                          |
| is               | 检查传入请求是否包含 `Content-Type` 头字段， 并且包含任意的 mime `type`。 如果没有请求主体，返回 `null`。 如果没有内容类型，或者匹配失败，则返回 `false`。 反之则返回匹配的 content-type。 |                                          |
| accepts          | 检查给定的 `type(s)` 是否可以接受，如果 `true`，返回最佳匹配，否则为 `false`。 `type` 值可能是一个或多个 mime 类型的字符串，如 `application/json`，扩展名称如 `json`，或数组 `["json", "html", "text/plain"]`。 |                                          |
| acceptsEncodings | 检查 `encodings` 是否可以接受，返回最佳匹配为 `true`，否则为 `false`。 请注意，您应该将`identity` 作为编码之一！ |                                          |
| acceptsCharsets  | 检查 `charsets` 是否可以接受，在 `true` 时返回最佳匹配，否则为 `false`。 |                                          |
| acceptsLanguages | 检查 `langs` 是否可以接受，如果为 `true`，返回最佳匹配，否则为 `false`。 |                                          |
| get              | 获取请求头信息。                                 |                                          |
| body             | 获取或设置响应主体                                |                                          |
| status           | 获取或设置响应状态                                |                                          |
| message          | 获取或设置响应的状态消息                             |                                          |
| length           | 获取或设置Content-Length。                     |                                          |
| type             | 获取或设置响应 `Content-Type` 通过 mime 字符串或文件扩展名 |                                          |
| headerSent       | 检查是否已经发送了一个响应头。 用于查看客户端是否可能会收到错误通知。      |                                          |
| redirect         | 执行 [302] 重定向到 `url`.                     |                                          |
| attachment       | 将 `Content-Disposition` 设置为 “附件” 以指示客户端提示下载。(可选)指定下载的 `filename`。 |                                          |
| set              | 用一个对象设置多个响应标头`fields`                    |                                          |
| append           | 用值 `val` 附加额外的标头 `field`。                |                                          |
| remove           | 删除标头 `field`。                            |                                          |
| lastModified     | 将 `Last-Modified` 标头返回为 `Date`, 如果存在。    |                                          |
| etag             | 获取或设置etag                                |                                          |
| cookies          | 获取或者设置cookie                             | ctx.cookies.get(name, [options])，ctx.cookies.set(name, value, [options]) |
| state            | 推荐的命名空间，用于传递信息                           |                                          |
|                  |                                          |                                          |

##### 扩展方法（ ctx ）

| 方法          | 说明                           | 示例                                       |
| ----------- | ---------------------------- | ---------------------------------------- |
| model       | 获取数据表模型                      | ctx.model("user").query({where: {id: 1}}).fetch(); |
| isStrToJson | 判断字符串能否转json对象               |                                          |
| isJsonToStr | 判断json对象能否转字符串               |                                          |
| uploadFile  | 上传文件到七牛云                     |                                          |
| strToArray  | 字符串转数组                       |                                          |
| bcrypt      | bcrypt加密                     |                                          |
| json        | 输出json对象                     |                                          |
| success     | 输出成功的json对象                  | ctx.success("hello", "获取数据成功");          |
| error       | 输入失败的json对象                  | ctx.error("error", "设置数据失败");            |
| post        | 获取post传参                     | ctx.post                                 |
| file        | 获取文件上传的信息                    | ctx.file                                 |
| appid       | 获取当前应用的appid                 |                                          |
| appkey      | 获取当前应用的appkey                |                                          |
| dev         | 获取当前应用的开发者状态                 |                                          |
| log         | 输出日志到控制台（请求日志，错误日志会自动输出到控制台） | ctx.log("hello");                        |
|             |                              |                                          |

##### 内置方法（ module ）

| 方法/变量              | 说明                                       | 示例                         |
| ------------------ | ---------------------------------------- | -------------------------- |
| module             | 加载模块，目前支持 assert，path，crypto，querystring，url，util，string_decoder，buffer，http，https | const path = module.path   |
| ajv                | ajv是一个基于JSON-Schema的依赖包，他可以将我们定义的Schema格式作为参数生成一个对象，使用这个对象的构造函数可以用于检测数据的合法性，除此之外还能够自定义directive来产生我们需要的format。 |                            |
| axios              | axios 是一个基于 promise 的 HTTP 库             | module.axios.get("xxx")    |
| bcrypt             | bcrypt 是一个加密算法                           |                            |
| bluebird           | bluebird 是一个promise库                     |                            |
| jsonwebtoken / jwt | jwt是JSON风格轻量级的授权和身份认证规范 可实现无状态、分布式的应用授权。 |                            |
| lodash / _         | lodash 是一个具有一致接口、模块化、高性能等特性的工具库          | module._.isString("1")     |
| md5                | md5 是一个加密算法                              | module.md5                 |
| moment             | moment日期处理类库                             |                            |
| uuid               | bcrypt 是一个生成随机ID的库                       |                            |
| qiniu              | qiniu 是一个七牛云库                            |                            |
| wechat             | wechat 是一个微信公众号自动回复的类库                   |                            |
| wechatPay          | wechatPay 是一个微信支付的类库                     |                            |
| WechatApi          | WechatApi 是一个微信公众号接口类库                   |                            |
| WechatOauth        | WechatOauth 是一个微信网页授权的类库                 |                            |
| aliPay             | aliPay 是一个支付宝支付的类库                       |                            |
| fse                | fse 是一个文件操作的类库，可操作【文件-网站】文件，支持 copy，emptyDir，ensureFile，ensureDir，ensureLink，ensureSymlink，mkdirs，move，outputFile，outputJson，pathExists，readJson，readFile，remove，writeJson | module.fse.mkdirs("hello") |
| redis              | redis 是一个缓存操作的类库。lock，unlock，delAll，cache; del，dump，exists，expire，expireat，keys，migrate，move，object，persist，pexpire，pexpireat，pttl，randomkey，rename，renamenx，restore，sort，ttl，type，scan; append，bitcount，bitop，decr，decrby，get，getbit，getrange，getset，incr，incrby，incrbyfloat，mget，mset，msetnx，psetex，set，setbit，setex，setnx，setrange，strlen; hdel，hexists，hget，hgetall，hincrby，hincrbyfloat，hkeys，hlen，hmget，hmset，hset，hsetnx，hvals，hscan; blpop，brpop，brpoplpush，lindex，linsert，llen，lpop，lpush，lpushx，lrange，lrem，lset，ltrim，rpop，rpoplpush，rpush，rpushx; sadd，scard，sdiff，sdiffstore，sinter，sinterstore，sismember，smembers，smove，spop，srandmember，srem，sunion，sunionstore，sscan，sorted; zadd，zcard，zcount，zincrby，zrange，zrangebyscore，zrank，zrem，zremrangebyrank，zremrangebyscore，zrevrange，zrevrangebyscore，zrevrank，zscore，zunionstore，zinterstore，zscan; discard，exec，multi，unwatch，watch                                        |  module.redis.lock("key")                          |
|                    |                                          |                            |

模型示例

> 提示：qb调用函数，同请求参数。

```js
async function hello(ctx, next, module) {
    const options = {
        withRelated: ["order", {
            "order": (qb) => {
                qb.where("other_id", "5");
            }
        }], // 关联查询
      	withDeleted: true, // 查询已删除的数据
      	withRedisKey: ctx.url // 支持redis缓存
    };
  	
  	// 查询数据（fetch查询1条数据。fetchAll查询所有的数据，默认是20条。fetchPage分页查询）
    const user = await ctx
        .model("user")
        .query(qb => {
            // qb调用函数，同请求参数
            qb.where("other_id", "5");
            qb.orWhere("other_id", ">", "5");
        })
        .fetch(options);

    // 新增/更新数据
    await ctx
        .model("user")
        .forge({
        	"id": 1, // 传递id为更新数据，不传递id为新增数据
        	"username": "1111"
        })
        .save();

    // 删除数据
    await ctx
        .model("user")
        .forge({
        	"id": 1 // 删除id等于1的数据
        })
        .destroy();

  	ctx.success(user);
}
```

云函数示例

```js
async function hello(ctx, next, module) {
  	ctx.body = "hello world!";
}
```

```
方法：
GET | POST | PUT | PATCH | DELETE

链接 ：
https://baas.qingful.com/2.0/class/public/function/hello

说明：
云函数hello链接
```

#### Hook模版

云函数钩子，数据表可设置可不设置，当数据表设置时，请求对应分组下的数据表时触发，不设置时请求分组下的任一数据表触发，作用类似于中间件。

```js
async function (ctx, next, module ) {
    // ToDo
}
```

#### Global模版

公共方法，可在Function和Hook中调用，例如：const res = await ctx.run("hello", 1, 2, 3)。

```js
async function __[函数名]__(ctx, next, module, 参数如a,b,c) {
    // ToDo
}
```

#### 计划任务示例

```js
async function hello(ctx, next, module) {
  	console.log("hello world!");
}
```

```
方法：
GET | POST | PUT | PATCH | DELETE

链接 ：
https://baas.qingful.com/2.0/class/public/function/hello/schedule/start

说明：
云函数hello计划任务开始链接

链接 ：
https://baas.qingful.com/2.0/class/public/function/hello/schedule/cancel

说明：
云函数hello计划任务取消链接
```


