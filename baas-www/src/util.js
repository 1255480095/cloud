import axios from "axios";
import avater from "./images/avater.png";
import avater1024 from "./images/avater-1024.png";
import { Modal as $Modal } from "iview";

// axios.defaults.baseURL = "http://192.168.31.101:8001";
axios.defaults.baseURL = "http://home.dev.qingful.com";

export default {
    wrapComponent(required) {
        return required.catch(err => {
            console.error("网络链接失败");
            $Modal.error({
                title: "错误信息",
                content: "<p>当前请求失败，建议您刷新页面或者稍后重试</p>",
                onOk: () => {
                    location.reload();
                }
            });
        });
    },

    getFile(file) {
        if (file) {
            return (
                axios.defaults.baseURL +
                `/uploads/${file.savepath}/${file.savename}`
            );
        }
        return "";
    },

    getAvater(file, type = "") {
        if (file && file.id) {
            return this.getFile(file);
        } else {
            if (type == "1024") {
                return avater1024;
            } else {
                return avater;
            }
        }
    },
    getLogo() {
        return "https://api.qingful.com/home/public/logo";
    },
    getCoryright() {
        let hosts = location.hostname.split(".");
        hosts.shift();
        return hosts.join(".");
    },
    getUrl(url) {
        if (url) {
            return axios.defaults.baseURL + url;
        }
        return "";
    },

    // get storage() {
    //     return storage.onConnect().then(() => {
    //         return storage;
    //     });
    // },

    alipayTrade(id) {
        location.href =
            axios.defaults.baseURL +
            `/pay/alipay?id=${id}&redirect=${encodeURIComponent(
                location.href
            )}&token=${axios.defaults.headers.common["Token"]}`;
    },

    isHttpUrl(url) {
        if (url.substr(0, 7) == "http://" || url.substr(0, 8) == "https://") {
            return true;
        }
        return false;
    },
    isFn(fn) {
        if (typeof fn === "function") {
            return true;
        }
        return false;
    },

    isStepInStr(str, step = "/") {
        if (str.indexOf(step) != 0) {
            return false;
        }
        return true;
    },

    format(time) {
        if (time) {
            const y = new Date(time).getFullYear();
            let m = new Date(time).getMonth() + 1;
            m = m < 10 ? "0" + m : m;
            let d = new Date(time).getDate();
            d = d < 10 ? "0" + d : d;

            let h = new Date(time).getHours();
            h = h < 10 ? "0" + h : h;
            let mi = new Date(time).getMinutes();
            mi = mi < 10 ? "0" + mi : mi;
            let s = new Date(time).getSeconds();
            s = s < 10 ? "0" + s : s;
            return y + "-" + m + "-" + d + " " + h + ":" + mi + ":" + s;
        }
        return "";
    },

    fetch(url, params) {
        if (params) {
            return axios
                .post(url, params)
                .then(res => {
                    if (res && res.data.code) {
                        return res;
                    } else {
                        if (res.data.msg || res.data.data) {
                            this.$Message.info(res.data.msg || res.data.data);
                        }
                    }
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 401) {
                            const module = location.pathname.split("/")[1];
                            if (module == "home") {
                                if (
                                    error.response.data.msg ==
                                    "Token Unauthorized"
                                ) {
                                    location.href = `/home/public/login`;
                                    return;
                                }
                                if (
                                    error.response.data.msg ==
                                    "Token Unauthorized"
                                ) {
                                    location.href = `/home/public/login`;
                                    return;
                                }
                                if (
                                    error.response.data.msg ==
                                    "baasToken Unauthorized"
                                ) {
                                    location.href = `/home/dashboard/app`;
                                    return;
                                }
                                if (
                                    error.response.data.msg ==
                                    "baasToken Unauthorized"
                                ) {
                                    location.href = `/home/dashboard/app`;
                                    return;
                                }
                            }

                            if (module == "admin") {
                                location.href = `/admin/public/login`;
                            }
                        }
                    } else {
                        console.log("Error", error.message);
                    }
                });
        }

        return axios
            .get(url)
            .then(res => {
                if (res && res.data.code) {
                    return res;
                } else {
                    if (res.data.msg || res.data.data) {
                        return res;
                        // this.$Message.info(res.data.msg || res.data.data);
                    }
                }
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 401) {
                        const module = location.pathname.split("/")[1];
                        if (module == "home") {
                            if (
                                error.response.data.msg == "Token Unauthorized"
                            ) {
                                location.href = `/home/public/login`;
                                return;
                            }
                            if (
                                error.response.data.msg == "Token Unauthorized"
                            ) {
                                location.href = `/home/public/login`;
                                return;
                            }
                            if (
                                error.response.data.msg ==
                                "baasToken Unauthorized"
                            ) {
                                location.href = `/home/dashboard/app`;
                                return;
                            }
                            if (
                                error.response.data.msg ==
                                "baasToken Unauthorized"
                            ) {
                                location.href = `/home/dashboard/app`;
                                return;
                            }
                        }

                        if (module == "admin") {
                            location.href = `/admin/public/login`;
                        }
                    }
                } else {
                    console.log("Error", error.message);
                }
            });
    },
    uploadHeaders() {
        return {
            Token: axios.defaults.headers.common["Token"],
            BaasToken: axios.defaults.headers.common["BaasToken"]
        };
    },

    handleFormatError(file) {
        this.$Notice.warning({
            title: "文件格式不正确",
            desc: "文件 " + file.name + " 格式不正确，请上传 jpg 或 png 格式的图片。"
        });
    },

    handleMaxSize(file) {
        this.$Notice.warning({
            title: "超出文件大小限制",
            desc: "文件 " + file.name + " 太大，不能超过 1M。"
        });
    }
};
